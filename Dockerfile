FROM debian:stable

MAINTAINER Pascal Chenevas-Paule <pascal.chenevas.gth@gmail.com>

#SOS Jobscheduler download URL
ENV SOS_JS_URL https://download.sos-berlin.com/JobScheduler.1.12/jobscheduler_linux-x64.1.12.7.tar.gz
ENV SOS_JOC_URL https://download.sos-berlin.com/JobScheduler.1.12/joc_linux.1.12.7.tar.gz
ENV SOS_AGENT_URL https://download.sos-berlin.com/JobScheduler.1.12/jobscheduler_unix_universal_agent.1.12.7.tar.gz

#add packages - needed for debian
RUN apt-get update&&\
        apt-get -y install openjdk-8-jdk nano grep libc6 curl tar bash sed mysql-client

#download and install scheduler
RUN curl -o /root/jobscheduler.tar.gz $SOS_JS_URL
RUN mkdir /root/install && tar xzvf /root/jobscheduler.tar.gz -C /root/install --strip-components=1
COPY scheduler_install.xml /root/install/scheduler_install.xml

#download job scheduler agent
RUN curl -o /root/jobscheduler_unix_universal_agent.tar.gz $SOS_AGENT_URL
RUN tar xzvf /root/jobscheduler_unix_universal_agent.tar.gz

#install
COPY startup_scheduler.sh /opt/startup_scheduler.sh

#expose scheduler ports
EXPOSE 40444 48444 4444 4446 40446 4445 59999

#start wrapper script
RUN chmod +x /opt/startup_scheduler.sh
CMD ["bash","/opt/startup_scheduler.sh"]
CMD ["/root/install/jobscheduler_agent/bin/jobscheduler_agent.sh", "start_docker"]
